import os
import threading
import cv2
from deepface import DeepFace


class VisionModule:
    def __init__(self):
        # Initialize camera
        self.cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

        # Initialize counter
        self.counter = 0

        # Load reference image
        script_dir = os.path.dirname(os.path.abspath(__file__))
        #self.reference_img_path = os.path.join(script_dir, "reference.jpg")
        self.reference_img_path = os.path.join(script_dir, "../../data/family/phillip/pic/reference.jpg")
        self.reference_img = cv2.imread(self.reference_img_path)
        self.reference_img_rgb = cv2.cvtColor(self.reference_img, cv2.COLOR_BGR2RGB) if self.reference_img is not None else None

        # Flag for face match
        self.face_match = False

        # Start a separate thread for face verification
        self.thread = threading.Thread(target=self.run_verification, daemon=True)
        self.thread.start()

    def run_verification(self):
        while True:
            ret, frame = self.cap.read()

            if ret:
                if self.counter % 30 == 0:
                    try:
                        # Perform face verification directly without threading
                        result = DeepFace.verify(frame, self.reference_img_rgb)
                        self.face_match = result['verified']
                    except ValueError:
                        self.face_match = False

                self.counter += 1

    def run(self):
        print("Vision module starting")

        while True:
            ret, frame = self.cap.read()

            if ret:
                if self.face_match:
                    cv2.putText(frame, "MATCH!", (20, 450), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 3)
                else:
                    cv2.putText(frame, "NO MATCH!", (20, 450), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 3)

                cv2.imshow('video', frame)

            key = cv2.waitKey(1)
            if key == ord('q'):
                break

        cv2.destroyAllWindows()
        self.cap.release()