from datetime import datetime
import speech_recognition as sr
import pyttsx3
import webbrowser
import wikipedia


class SpeechModule:
    def __init__(self):
        self.engine = pyttsx3.init()
        self.voices = self.engine.getProperty('voices')
        self.engine.setProperty('voice', self.voices[0].id)
        self.activation_word = 'computer'
        self.listener = sr.Recognizer()

    def speak(self, text, rate=120):
        self.engine.setProperty('rate', rate)
        self.engine.say(text)
        self.engine.runAndWait()

    def parse_command(self):
        print('Listening for a command')
        with sr.Microphone() as source:
            self.listener.pause_threshold = 2
            input_speech = self.listener.listen(source)

        try:
            print('Recognizing speech...')
            query = self.listener.recognize_google(input_speech, language='en_gb').lower()
            print(f'The input speech was: {query}')
            return query
        except Exception as exception:
            print('I did not quite catch that')
            self.speak('I did not quite catch that')
            print(exception)
            return 'None'

    def search_wikipedia(self, query=''):
        search_results = wikipedia.search(query)
        if not search_results:
            print('No Wikipedia result')
            return 'No result received'
        try:
            wiki_page = wikipedia.page(search_results[0])
        except wikipedia.DisambiguationError as error:
            wiki_page = wikipedia.page(error.options[0])
        print(wiki_page.title)
        wiki_summary = str(wiki_page.summary)
        return wiki_summary

    def run(self):
        self.speak('Systems ready')
        while True:
            print("Listening")
            command = self.parse_command().split()
            if command and command[0] == self.activation_word:
                command.pop(0)

                if command:
                    action = command.pop(0)

                    if action == 'say':
                        if 'hello' in command:
                            self.speak('Greetings, all.')
                        else:
                            speech_text = ' '.join(command)
                            self.speak(speech_text)

                    elif action == 'wikipedia':
                        query_text = ' '.join(command)
                        self.speak('Querying the universal databank.')
                        self.speak(self.search_wikipedia(query_text))

                    elif action == 'log':
                        self.speak('Ready to record your note')
                        new_note = self.parse_command().lower()
                        now = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
                        with open(f'note_{now}.txt', 'w') as new_file:
                            new_file.write(new_note)
                        self.speak('Note written')

                    elif action == 'exit':
                        self.speak('Goodbye')
                        break
