from src.speech.speechEngine import SpeechModule
from src.vision.visionEngine import VisionModule
import threading


def main():
    print("Robot starting...")
    # Create an instance of the SpeechModule
    speech_module = SpeechModule()
    vision_module = VisionModule()
    # Create threads for the SpeechModule and VisionModule
    #speech_thread = threading.Thread(target=speech_module.run())
    vision_thread = threading.Thread(target=vision_module.run())

    # Start the threads

    vision_thread.start()

    # Wait for both threads to finish
    # speech_thread.join()
    # vision_thread.join()
    # print("Threads done")
    # speech_thread.join()


if __name__ == "__main__":
    main()
